//requires
const   express = require('express'),
        path = require('path'),
        SocketIO = require('socket.io');
/**
 * @description port and rute
 */
const port = 3001;


/**
 * @description load app 
 */
var app = express();
app.set('port', port);
app.use(express.urlencoded({extended: false}));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use(express.static(path.join(__dirname,'page')))

/**
 * app.listen
 * @description enpoint listen 
 */
const server = app.listen(port, function() {
    console.log('ok ' + port);
});

const io = SocketIO(server)

io.on('connection',(socket)=>{
    console.log('connection ', socket.id);
    socket.on('peticionJuego',(json)=>{
        io.to(json.id).emit('peticionJuego',json)
    })
    socket.on('aceptarJuego',(json)=>{
        io.to(json.idPeticion).emit('aceptarJuego',json)
        
        const juego = {
            ...json,
            turno:json.idPeticion,
            tabla:[
                ["#fff","#fff","#fff"],
                ["#fff","#fff","#fff"],
                ["#fff","#fff","#fff"]
            ]
        }
        io.to(json.id).emit('loadJuego',juego)
        io.to(json.idPeticion).emit('loadJuego',juego)

    })
    socket.on('cancelarJuego',(json)=>{
        io.to(json.idPeticion).emit('cancelarJuego',json)
    })
    socket.on('click',(json)=>{
        const turno = json.turno
        if(turno == json.id){
            json.turno = json.idPeticion
        }else{
            json.turno = json.id
        }
        io.to(json.id).emit('loadJuego',json)
        io.to(json.idPeticion).emit('loadJuego',json)
    })

})